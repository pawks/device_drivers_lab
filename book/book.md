---
title: "Device Drivers for Dummies"
author: [S Pawan Kumar, CED16I043]
date: "2020-06-03"
titlepage: true
table-use-row-colors: true
classoption: [oneside]
toc-own-page: true
book: true
first-chapter: 0
listings-disable-line-numbers: true
...

# Acknowledgements

\vfill
\begin{center}
\begin{minipage}{.6\textwidth}
\textit{Sincere thanks to my teacher for providing me the opportunity to compile this book and culminate my learning experience in the course.} 
\end{minipage}
\end{center}
\vfill 
\clearpage

# Introduction to Device Drivers

One of the many advantages of free operating systems, as typified by Linux, is that their internals are open for all to view. The operating system, once a dark and mysterious area whose code was restricted to a small number of programmers, can now be readily examined, understood, and modified by anybody with the requisite skills. Linux has helped to democratize operating systems. The Linux kernel remains a large and complex body of code, however, and would-be kernel hackers need an entry point where they can approach the code without being overwhelmed by complexity. Often, device drivers provide that gateway.

Device drivers take on a special role in the Linux kernel. They are distinct “black boxes” that make a particular piece of hardware respond to a well-defined internal programming interface; they hide completely the details of how the device works. User activities are performed by means of a set of standardized calls that are independent of the specific driver; mapping those calls to device-specific operations that act on real hardware is then the role of the device driver. This programming interface is such that drivers can be built separately from the rest of the kernel and “plugged in” at runtime when needed. This modularity makes Linux drivers easy to write, to the point that there are now hundreds of them available.

There are a number of reasons to be interested in the writing of Linux device drivers. The rate at which new hardware becomes available (and obsolete!) alone guarantees that driver writers will be busy for the foreseeable future. Individuals may need to know about drivers in order to gain access to a particular device that is of interest to them. Hardware vendors, by making a Linux driver available for their products, can add the large and growing Linux user base to their potential markets. And the open source nature of the Linux system means that if the driver writer wishes, the source to a driver can be quickly disseminated to millions of users.

This book provides a brief introduction to device drivers and provides starting code base and understanding for a few types of device drivers. As you learn to write drivers, you find out a lot about the Linux kernel in general; this may help you understand how your machine works and why things aren’t always as fast as you expect or don’t do quite what you want. 

This chapter doesn’t actually get into writing code. However, we introduce some background concepts about the Linux kernel that you’ll be glad you know later, when we do launch into programming.

**Note:** All programs in this book have been tested using the v5.4 of the Linux Kernel. The programs might work with other kernel versions. You can check the version of your kernel using:
``` bash
$ cat /proc/version
```

## The Role of the Device Driver

As a programmer, you are able to make your own choices about your driver, and choose an acceptable trade-off between the programming time required and the flexibility of the result. Though it may appear strange to say that a driver is “flexible,” we like this word because it emphasizes that the role of a device driver is providing mechanism, not policy.

The distinction between mechanism and policy is one of the best ideas behind the Unix design. Most programming problems can indeed be split into two parts: “what capabilities are to be provided” (the mechanism) and “how those capabilities can be used” (the policy). If the two issues are addressed by different parts of the program, or even by different programs altogether, the software package is much easier to develop and to adapt to particular needs.

For example, Unix management of the graphic display is split between the X server, which knows the hardware and offers a unified interface to user programs, and the window and session managers, which implement a particular policy without knowing anything about the hardware. People can use the same window manager on different hardware, and different users can run different configurations on the same workstation. Even completely different desktop environments, such as KDE and GNOME, can coexist on the same system. Another example is the layered structure of TCP/IP networking: the operating system offers the socket abstraction, which implements no policy regarding the data to be transferred, while different servers are in charge of the services (and their associated policies). Moreover, a server like ftpd provides the file transfer mechanism, while users can use whatever client they prefer; both command-line and graphic clients exist, and anyone can write a new user interface to transfer files.

Where drivers are concerned, the same separation of mechanism and policy applies. The floppy driver is policy free—its role is only to show the diskette as a continuous array of data blocks. Higher levels of the system provide policies, such as who may access the floppy drive, whether the drive is accessed directly or via a filesystem, and whether users may mount filesystems on the drive. Since different environments usually need to use hardware in different ways, it’s important to be as policy free as possible.

When writing drivers, a programmer should pay particular attention to this fundamental concept: write kernel code to access the hardware, but don’t force particular policies on the user, since different users have different needs. The driver should deal with making the hardware available, leaving all the issues about how to use the hardware to the applications. A driver, then, is flexible if it offers access to the hardware capabilities without adding constraints. Sometimes, however, some policy decisions must be made. For example, a digital I/O driver may only offer byte-wide access to the hardware in order to avoid the extra code needed to handle individual bits.

You can also look at your driver from a different perspective: it is a software layer that lies between the applications and the actual device. This privileged role of the driver allows the driver programmer to choose exactly how the device should appear: different drivers can offer different capabilities, even for the same device. The actual driver design should be a balance between many different considerations. For instance, a single device may be used concurrently by different programs, and the driver programmer has complete freedom to determine how to handle concurrency. You could implement memory mapping on the device independently of its hardware capabilities, or you could provide a user library to help application programmers implement new policies on top of the available primitives, and so forth. One major consideration is the trade-off between the desire to present the user with as many options as possible and the time you have to write the driver, as well as the need to keep things simple so that errors don’t creep in.

Policy-free drivers have a number of typical characteristics. These include support for both synchronous and asynchronous operation, the ability to be opened multiple times, the ability to exploit the full capabilities of the hardware, and the lack of software layers to “simplify things” or provide policy-related operations. Drivers of this sort not only work better for their end users, but also turn out to be easier to write and maintain as well. Being policy-free is actually a common target for software designers.

Many device drivers, indeed, are released together with user programs to help with configuration and access to the target device. Those programs can range from simple utilities to complete graphical applications. Examples include the tunelp program, which adjusts how the parallel port printer driver operates, and the graphical cardctl utility that is part of the PCMCIA driver package. Often a client library is provided as well, which provides capabilities that do not need to be implemented as part of the driver itself.

The scope of this book is the kernel, so we try not to deal with policy issues or with application programs or support libraries. Sometimes we talk about different policies and how to support them, but we won’t go into much detail about programs using the device or the policies they enforce. You should understand, however, that user programs are an integral part of a software package and that even policy-free packages are distributed with configuration files that apply a default behavior to the underlying mechanisms.

## Splitting the Kernel
In a Unix system, several concurrent processes attend to different tasks. Each process asks for system resources, be it computing power, memory, network connectivity, or some other resource. The kernel is the big chunk of executable code in charge of handling all such requests. Although the distinction between the different kernel tasks isn’t always clearly marked, the kernel’s role can be split into the following parts:

* *Process management*  
The kernel is in charge of creating and destroying processes and handling their connection to the outside world (input and output). Communication among different processes (through signals, pipes, or interprocess communication primitives) is basic to the overall system functionality and is also handled by the kernel. In addition, the scheduler, which controls how processes share the CPU, is part of process management. More generally, the kernel’s process management activity implements the abstraction of several processes on top of a single CPU or a few of them.  
* *Memory Management*  
The computer’s memory is a major resource, and the policy used to deal with it is a critical one for system performance. The kernel builds up a virtual addressing space for any and all processes on top of the limited available resources. The different parts of the kernel interact with the memory-management subsystem through a set of function calls, ranging from the simple malloc/free pair to much more complex functionalities.  
* *Filesystems*  
Unix is heavily based on the filesystem concept; almost everything in Unix can be treated as a file. The kernel builds a structured filesystem on top of unstructured hardware, and the resulting file abstraction is heavily used throughout the whole system. In addition, Linux supports multiple filesystem types, that is, different ways of organizing data on the physical medium. For example, disks may be formatted with the Linux-standard ext3 filesystem, the commonly used FAT filesystem or several others.  
* *Device Control*  
Almost every system operation eventually maps to a physical device. With the exception of the processor, memory, and a very few other entities, any and all device control operations are performed by code that is specific to the device being addressed. That code is called a *device driver*. The kernel must have embedded in it a device driver for every peripheral present on a system, from the hard drive to the keyboard and the tape drive.  
* *Networking*  
Networking must be managed by the operating system, because most network operations are not specific to a process: incoming packets are asynchronous events. The packets must be collected, identified, and dispatched before a process takes care of them. The system is in charge of delivering data packets across program and network interfaces, and it must control the execution of programs according to their network activity. Additionally, all the routing and address resolution issues are implemented within the kernel.  


## Loadable Modules
One of the good features of Linux is the ability to extend at runtime the set of features offered by the kernel. This means that you can add functionality to the kernel (and remove functionality as well) while the system is up and running.

Each piece of code that can be added to the kernel at runtime is called a module . The Linux kernel offers support for quite a few different types (or classes) of modules, including, but not limited to, device drivers. Each module is made up of object code (not linked into a complete executable) that can be dynamically linked to the running kernel by the *insmod* program and can be unlinked by the *rmmod* program.

## Class of Device Drivers and Modules
The Linux way of looking at devices distinguishes between three fundamental device types. Each module usually implements one of these types, and thus is classifiable as a char module, a block module, or a network module. This division of modules into different types, or classes, is not a rigid one; the programmer can choose to build huge modules implementing different drivers in a single chunk of code. Good programmers, nonetheless, usually create a different module for each new functionality they implement, because decomposition is a key element of scalability and extendability.

The three classes are:

* *Character Devices*  
A character (char) device is one that can be accessed as a stream of bytes (like a file); a char driver is in charge of implementing this behavior. Such a driver usually implements at least the open, close, read, and write system calls. The text console (/dev/console) and the serial ports (/dev/ttyS0 and friends) are examples of char devices, as they are well represented by the stream abstraction. Char devices are accessed by means of filesystem nodes, such as /dev/tty1 and /dev/lp0. The only relevant difference between a char device and a regular file is that you can always move back and forth in the regular file, whereas most char devices are just data channels, which you can only access sequentially. There exist, nonetheless, char devices that look like data areas, and you can move back and forth in them; for instance, this usually applies to frame grabbers, where the applications can access the whole acquired image using *mmap* or *lseek*.  
* *Block Devices*  
Like char devices, block devices are accessed by filesystem nodes in the /dev directory. A block device is a device (e.g., a disk) that can host a filesystem. In most Unix systems, a block device can only handle I/O operations that transfer one or more whole blocks, which are usually 512 bytes (or a larger power of two) bytes in length. Linux, instead, allows the application to read and write a block device like a char device—it permits the transfer of any number of bytes at a time. As a result, block and char devices differ only in the way data is managed internally by the kernel, and thus in the kernel/driver software interface. Like a char device, each block device is accessed through a filesystem node, and the difference between them is transparent to the user. Block drivers have a completely different interface to the kernel than char drivers.  
* *Network interfaces*  
Any network transaction is made through an interface, that is, a device that is able to exchange data with other hosts. Usually, an interface is a hardware device, but it might also be a pure software device, like the loopback interface. A network interface is in charge of sending and receiving data packets, driven by the network subsystem of the kernel, without knowing how individual transactions map to the actual packets being transmitted. Many network connections (especially those using TCP) are stream-oriented, but network devices are, usually, designed around the transmission and receipt of packets. A network driver knows nothing about individual connections; it only handles packets.  

Not being a stream-oriented device, a network interface isn’t easily mapped to a node in the filesystem, as /dev/tty1 is. The Unix way to provide access to interfaces is still by assigning a unique name to them (such as eth0), but that name doesn’t have a corresponding entry in the filesystem. Communication between the kernel and a network device driver is completely different from that used with char and block drivers. Instead of read and write, the kernel calls functions related to packet transmission.

There are other ways of classifying driver modules that are orthogonal to the above device types. In general, some types of drivers work with additional layers of kernel support functions for a given type of device. For example, one can talk of universal serial bus (USB) modules, serial modules, SCSI modules, and so on. Every USB device is driven by a USB module that works with the USB subsystem, but the device itself shows up in the system as a char device (a USB serial port, say), a block device (a USB memory card reader), or a network device (a USB Ethernet interface).

Other classes of device drivers have been added to the kernel in recent times, including FireWire drivers and I2O drivers. In the same way that they handled USB and SCSI drivers, kernel developers collected class-wide features and exported them to driver implementers to avoid duplicating work and bugs, thus simplifying and strengthening the process of writing such drivers.

In addition to device drivers, other functionalities, both hardware and software, are modularized in the kernel. One common example is filesystems. A filesystem type determines how information is organized on a block device in order to represent a tree of directories and files. Such an entity is not a device driver, in that there’s no explicit device associated with the way the information is laid down; the filesystem type is instead a software driver, because it maps the low-level data structures to high-level data structures. It is the filesystem that determines how long a filename can be and what information about each file is stored in a directory entry. The filesystem module must implement the lowest level of the system calls that access directories and files, by mapping filenames and paths (as well as other information, such as access modes) to data structures stored in data blocks. Such an interface is completely independent of the actual data transfer to and from the disk (or other medium), which is accomplished by a block device driver.

If you think of how strongly a Unix system depends on the underlying filesystem, you’ll realize that such a software concept is vital to system operation. The ability to decode filesystem information stays at the lowest level of the kernel hierarchy and is of utmost importance; even if you write a block driver for your new CD-ROM, it is useless if you are not able to run ls or cp on the data it hosts. Linux supports the concept of a filesystem module, whose software interface declares the different operations that can be performed on a filesystem inode, directory, file, and superblock. It’s quite unusual for a programmer to actually need to write a filesystem module, because the official kernel already includes code for the most important filesystem types.

## Security Issues
Any security check in the system is enforced by kernel code. If the kernel has security holes, then the system as a whole has holes. In the official kernel distribution, only an authorized user can load modules; the system call init_module checks if the invoking process is authorized to load a module into the kernel. Thus, when running an official kernel, only the superuser,[1] or an intruder who has succeeded in becoming privileged, can exploit the power of privileged code.

When possible, driver writers should avoid encoding security policy in their code. Security is a policy issue that is often best handled at higher levels within the kernel, under the control of the system administrator. There are always exceptions, however. As a device driver writer, you should be aware of situations in which some types of device access could adversely affect the system as a whole and should provide adequate controls. For example, device operations that affect global resources (such as setting an interrupt line), which could damage the hardware (loading firmware, for example), or that could affect other users (such as setting a default block size on a tape drive), are usually only available to sufficiently privileged users, and this check must be made in the driver itself.

Driver writers must also be careful, of course, to avoid introducing security bugs. The C programming language makes it easy to make several types of errors. Many current security problems are created, for example, by buffer overrun errors, in which the programmer forgets to check how much data is written to a buffer, and data ends up written beyond the end of the buffer, thus overwriting unrelated data. Such errors can compromise the entire system and must be avoided. Fortunately, avoiding these errors is usually relatively easy in the device driver context, in which the interface to the user is narrowly defined and highly controlled.

Some other general security ideas are worth keeping in mind. Any input received from user processes should be treated with great suspicion; never trust it unless you can verify it. Be careful with uninitialized memory; any memory obtained from the kernel should be zeroed or otherwise initialized before being made available to a user process or device. Otherwise, information leakage (disclosure of data, passwords, etc.) could result. If your device interprets data sent to it, be sure the user cannot send anything that could compromise the system. Finally, think about the possible effect of device operations; if there are specific operations (e.g., reloading the firmware on an adapter board or formatting a disk) that could affect the system, those operations should almost certainly be restricted to privileged users.

Be careful, also, when receiving software from third parties, especially when the kernel is concerned: because everybody has access to the source code, everybody can break and recompile things. Although you can usually trust precompiled kernels found in your distribution, you should avoid running kernels compiled by an untrusted friend—if you wouldn’t run a precompiled binary as root, then you’d better not run a precompiled kernel. For example, a maliciously modified kernel could allow anyone to load a module, thus opening an unexpected back door via init_module.

Note that the Linux kernel can be compiled to have no module support whatsoever, thus closing any module-related security holes. In this case, of course, all needed drivers must be built directly into the kernel itself. It is also possible, with 2.2 and later kernels, to disable the loading of kernel modules after system boot via the capability mechanism.

## Licence Terms
Linux is licensed under Version 2 of the GNU General Public License (GPL), a document devised for the GNU project by the Free Software Foundation. The GPL allows anybody to redistribute, and even sell, a product covered by the GPL, as long as the recipient has access to the source and is able to exercise the same rights. Additionally, any software product derived from a product covered by the GPL must, if it is redistributed at all, be released under the GPL.

The main goal of such a license is to allow the growth of knowledge by permitting everybody to modify programs at will; at the same time, people selling software to the public can still do their job. Despite this simple objective, there’s a never-ending discussion about the GPL and its use. If you want to read the license, you can find it in several places in your system, including the top directory of your kernel source tree in the COPYING file.

If you want your code to go into the mainline kernel, or if your code requires patches to the kernel, you must use a GPL-compatible license as soon as you release the code. Although personal use of your changes doesn’t force the GPL on you, if you distribute your code, you must include the source code in the distribution—people acquiring your package must be allowed to rebuild the binary at will.


# Simple Device Driver

Linux has a monolithic kernel. For this reason, writing a device driver for Linux requires performing a combined compilation with the kernel. Another way around is to implement your driver as a kernel module, in which case you won’t need to recompile the kernel to add another driver. We’ll be concerned with this second option: kernel modules.

At its base, a module is a specifically designed object file. When working with modules, Linux links them to its kernel by loading them to its address space. The Linux kernel was developed using the C programming language and Assembler. C implements the main part of the kernel, and Assembler implements parts that depend on the architecture. Unfortunately, these are the only two languages we can use for writing Linux device drivers. We cannot use C++, which is used for the Microsoft Windows operating system kernel, because some parts of the Linux kernel source code – header files, to be specific – may include keywords from C++ (for example, delete or new), while in Assembler we may encounter lexemes such as ‘ : : ’.

We run the module code in the kernel context. This requires a developer to be very attentive, as it entails extra responsibilities: if a developer makes a mistake when implementing a user-level application, this will not cause problems outside the user application in most cases; but if a developer makes a mistake when implementing a kernel module, the consequences will be problems at the system level. Luckily for us, the Linux kernel has a nice feature of being resistant to errors in module code. When the kernel encounters non-critical errors (for example, null pointer dereferencing), you’ll see the oops message (insignificant malfunctions during Linux operation are called oops), after which the malfunctioning module will be unloaded, allowing the kernel and other modules to work as usual. In addition, you’ll be able to find a record in the kernel log that precisely describes this error. But be aware that continuing work after an oops message is not recommended, as doing so may lead to instability and kernel panic.

The kernel and its modules essentially represent a single program module – so keep in mind that a single program module uses a single global namespace. In order to minimize it, you must watch what is being exported by the module: exported global characters must be named uniquely (a commonly used workaround is to simply use the name of the module that’s exporting the characters as a prefix) and must be cut to the bare minimum.

In this chapter we look at the basics of device driver modules and following the traditional first step of greeting the world while learning any language/concept, we implement a simple driver which does nothing but print *Hello World!* in the kernel log.

## Hello World Program

``` C

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("Dual BSD/GPL");

MODULE_AUTHOR("S Pawan Kumar");

MODULE_DESCRIPTION("A simple example Linux module.");

MODULE_VERSION("0.01");

static int hello_init(void) 
{  
    printk(KERN_INFO "Hello world!\n");  
    return 0;
}

static void hello_exit(void) 
{  
    printk(KERN_INFO "Bye, cruel world\n");
}

module_init(hello_init);
module_exit(hello_exit);

```
* The *MODULE_LICENSE* macro specifies the license identifier for the module.
* The *MODULE_AUTHOR* macro supplies the author information for the module.
* the *MODULE_DESCRIPTION* and *MODULE_VERSION* specify a brief description and the version respectively.
* The only two things this module does is load and unload itself. To load a Linux driver, we call the *hello_init* function, and to unload it, we call the *hello_exit* function. The *module_init* and *module_exit* macros notify the kernel about driver loading and unloading. The actual functions *hello_init* and *hello_exit* can be given any name desired. However, in order for them to be identified as the corresponding loading and removing functions, they have to be passed as parameters to the macros *module_init* and *module_exit*. The *hello_init* and *hello_exit* functions must have identical signatures, which must be exactly as follows:
``` C  
    int init(void);  
    void exit(void);   
```
* If the module requires a certain kernel version and must include information on the version, we need to link the **linux/module.h** header file. Trying to load a module built for another kernel version will lead to the Linux operating system prohibiting its loading. There’s a reason for such behavior: updates to the kernel API are released quite often, and when you call a module function whose signature has been changed, you cause damage to the whole stack. The *module_init* and *module_exit* macros are declared in the **linux/init.h** header file.  
* The *printk* function has also been introduced. It is very similar to the well known *printf* apart from the fact that it only works inside the kernel. The *KERN_INFO* symbol shows the low priority of the message. In this way you get the message in the kernel system log files.

## Makefile

``` Makefile
obj-m += hello_world.o
target:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
    @make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
test:
	@sudo dmesg -C
	@sudo insmod hello_world.ko
	@sudo rmmod hello_world.ko
	@dmesg
```

* Compile

``` bash
$ make target
```

* Test
    * Loading a module -
        ``` 
        sudo insmod <ko file name> 
        ```
    * Removing a module -
        ``` 
        sudo rmmod <ko file name> 
        ```

``` bash  
$ make test  
[19235.591209] Hello, World!  
[19235.598742] Goodbye, World!  
```

# Writing a Simple Character Driver

We will now have a look at how to develop a simple character driver. This device will allow a character to be read from or written into it. This device, while normally not very useful, provides a very illustrative example since it is a complete driver; it’s also easy to implement, since it doesn’t interface to a real hardware device(besides the computer itself).To develop this driver, several new *#include* statements which appear frequently in device drivers need tobe added:

## Prelimnary code
``` C
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h> /* printk() */
#include <linux/slab.h> /* kmalloc() */
#include <linux/fs.h> /* everything... */
#include <linux/errno.h> /* error codes */
#include <linux/types.h> /* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h> /* O_ACCMODE */
#include <asm/uaccess.h> /* copy_from/to_user */

/* Module Information*/
MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("S Pawan Kumar");
MODULE_DESCRIPTION("A simple character device.");
MODULE_VERSION("0.01");

/* Declaration of memory.c functions */
int memory_open(struct inode *inode, struct file *filp);
int memory_release(struct inode *inode, struct file *filp);
ssize_t memory_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
ssize_t memory_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos);
void memory_exit(void);
int memory_init(void);

/* Structure that declares the usual file */
/* access functions */
struct file_operations memory_fops = {  
    read: memory_read,  
    write: memory_write,  
    open: memory_open,  
    release: memory_release
};

/* Declaration of the init and exit functions */
module_init(memory_init);
module_exit(memory_exit);

/* Global variables of the driver */
/* Major number */
int memory_major = 60;
/* Buffer to store data */
char *memory_buffer;

```

After the *#include* files, the functions that will be defined later are declared. The common functions which are typically used to manipulate files are declared in the definition of the *file_operations* structure. These will also be explained in detail later. Next, the initialization and exit functions(used when loading and removing the module) are declared to the kernel. Finally, the global variables of the driver are declared: one of them is the major number of the driver, the other is a pointer to a region in memory(*memory_buffer*), which will be used as storage for the driver data.

## Connecting the device with its files
In UNIX and Linux, devices are accessed from user space in exactly the same way as files are accessed. These device files are normally subdirectories of the /dev directory. To link normal files with a kernel module two numbers are used: *major number* and *minor number*. The *major number* is the one the kernel uses to link a file with its driver. The *minor number* is for internal use of the device and for simplicity it won’t be covered in this book. To achieve this, a device node file (which will be used to access the device driver) must be created, by typing the following command:
``` bash
$ sudo mknod /dev/memory c 60 0
```
In the above, *c* means that a char device is to be created, 60 is the *major number* and 0 is the *minor number*.

## Writing the Device I/O functions
### Initialisation
Within the driver, in order to link it with its corresponding /dev file in kernel space, the *register_chrdev* function is used. It is called with three arguments: major number, a string of characters showing the module name, and a *file_operations* structure which links the call with the file functions it defines.

``` C
int memory_init(void) 
{  
    int result;  
    
    /* Registering device */  
    result = register_chrdev(memory_major, "memory", &memory_fops);  
    if (result < 0) {    
    printk( KERN_ERR "memory: cannot obtain major number %d\n", memory_major);    
    return result;  
    }  
    
    /* Allocating memory for the buffer */  
    memory_buffer = kmalloc(1, GFP_KERNEL);   
    if (!memory_buffer) {     
    result = -ENOMEM;    
    goto fail;   
    }   
    memset(memory_buffer, 0, 1);  
    printk(KERN_INFO "Inserting memory module\n");   
    return 0;  
    fail:     
        memory_exit();     
        return result;
}
```

Also, note the use of the *kmalloc* function. This function is used for memory allocation of the buffer in the device driver which resides in kernel space. Its use is very similar to the well known *malloc* function.Finally, if registering the major number or allocating the memory fails, the module acts accordingly.

### Exit
In order to remove the module inside the *memory_exit* function, the function *unregsiter_chrdev* needs to be present. This will free the *major number* for the kernel.

``` C
void memory_exit(void) {  
    /* Freeing the major number */  
    unregister_chrdev(memory_major, "memory");  
    
    /* Freeing buffer memory */  
    if (memory_buffer) {    
        kfree(memory_buffer);  
    }  

    printk(KERN_INFO "Removing memory module\n");
}
```

The *kfree* function is used for releasing the memory acquired through *kmalloc* function and is very similar to the well known *free* function. The buffer memory is also freed in this function, in order to leave a clean kernel when removing the device driver.

### Openning the device as a file
The kernel space function, which corresponds to opening a file in user space ( *fopen* ), is the member *open* of the *file_operations* structure in the call to *register_chrdev*. In this case, it is the *memory_open* function. It takes as arguments: an inode structure, which sends information to the kernel regarding the *major number* and *minor number*; and a file structure with information relative to the different operations that can be performed on a file. Neither of these functions will be covered in depth within this book. When a file is opened, it’s normally necessary to initialize driver variables or reset the device. In this simple example, though, these operations are not performed.

``` C
int memory_open(struct inode *inode, struct file *filp) {  
    /* Success */  
    return 0;
}
```

### Closing the device as a file
The corresponding function for closing a file in user space ( *fclose* ) is the *release* member of the *file_operations* structure in the call to *register_chrdev*. In this particular case, it is the function *memory_release*, which has as arguments an inode structure and a file structure, just like before. When a file is closed, it’s usually necessary to free the used memory and any variables related to the opening of the device. But, once again, due to the simplicity of this example, none of these operations are performed.

``` C
int memory_release(struct inode *inode, struct file *filp) {  
    /* Success */  
    return 0;
}
```

## Device Operations
### Reading from the device
To read a device with the user function *fread* or similar, the member *read* of the *file_operations* structure is used in the call to *register_chrdev*. This time, it is the function *memory_read*. Its arguments are: a type file structure; a buffer ( *buf* ), from which the user space function ( *fread* ) will read; a counter with the number of bytes to transfer ( *count* ), which has the same value as the usual counter in the user space function ( *fread* ); and finally, the position of where to start reading the file ( *f_pos* ). In this simple case, the *memory_read* function transfers a single byte from the driver buffer( *memory_buffer* ) to user space with the function *raw_copy_to_user*.

``` C
ssize_t memory_read(struct file *filp, char *buf, size_t count, loff_t *f_pos) {   
    
    /* Transfering data to user space */   
    raw_copy_to_user(buf,memory_buffer,1);
    
    /* Changing reading position as best suits */   
    if (*f_pos == 0) {     
        *f_pos+=1;     
        return 1;   
    } 
    else {     
        return 0;   
    }
}
```

The reading position in the file ( *f_pos* ) is also changed. If the position is at the beginning of the file, it is increased by one and the number of bytes that have been properly read is given as a return value(1). If not at the beginning of the file, an end of file(0) is returned since the file only stores one byte.

### Writing to the device
To write to a device with the user function *fwrite* or similar, the member *write* of the *file_operations* structure is used in the call to *register_chrdev*. It is the function *memory_write*, in this particular example, which has the following as arguments: a type file structure; *buf*, a buffer in which the user space function ( *fwrite* ) will write; *count*, a counter with the number of bytes to transfer, which has the same values as the usual counter in the user space function (*fwrite*); and finally, *f_pos*, the position of where to start writing in the file.

``` C
ssize_t memory_write( struct file *filp, const char *buf, size_t count, loff_t *f_pos) {  
    char *tmp;  
    tmp=buf+count-1;  
    raw_copy_from_user(memory_buffer,tmp,1);  
    return 1;
}

```

In this case, the function *raw_copy_from_user* transfers the data from user space to kernel space.

## Compiling and testing

Makefile - 
``` Makefile
obj-m += memory.o
target:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
device:
	@sudo mknod /dev/memory c 60 0
clean:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
ins:
	@sudo dmesg -C
	@sudo insmod memory.ko
	@sudo chmod 666 /dev/memory
	@dmesg
	@sudo dmesg -C
rm:
	@sudo rmmod memory.ko
	@dmesg
test:
	echo -n abcdef > /dev/memory
	cat /dev/memory
	@echo -n "\n"
```

* Create the device node
``` bash
$ make device
```
* Compile and insert the device
``` bash
$ make target ins
make[1]: Entering directory '/usr/src/linux-headers-5.4.0-33-generic'
  Building modules, stage 2.
  MODPOST 1 modules
make[1]: Leaving directory '/usr/src/linux-headers-5.4.0-33-generic'
[ 3041.607156] Inserting memory module
```
* Test the device - Writing to or reading from /dev/memory
``` bash
$ make test
echo -n abcdef > /dev/memory
cat /dev/memory
f
```
* Remove the module
``` bash
$ make rm clean
[ 3244.941224] Removing memory module
make[1]: Entering directory '/usr/src/linux-headers-5.4.0-33-generic'
  CLEAN   /home/sharder/iiitdm/device_drivers_lab/chardev/Module.symvers
make[1]: Leaving directory '/usr/src/linux-headers-5.4.0-33-generic'
```

**Note:** You can delete the device file(/dev/memory) created using the *mknod* command just by using the *rm* command. A device node created by *mknod* is just a file that contains a device major and minor number. When you access that file the first time, Linux looks for a driver that advertises that major/minor and loads it. Your driver then handles all I/O with that file.

When you delete a device node, the usual Un*x file behavior aplies: Linux will wait until there are no more references to the file and then it will be deleted from disk.

Your driver doesn't really notice anything of this. Linux does not automatically unload modules. Your driver wil simply no longer receive requests do to anything. But it will be ready in case anybody recreates the device node. 

# Conclusion

We have looked at what are the steps to start writing linux device drivers and all the necessary steps to write a character device driver. The books mentioned in the bibliography offer an indepth experience into the linux kernel and contains all essential information required to understand and start writing modules for the linux kernel. This book aims to offer a starting point for writing device drivers and I hope it fulfilled it's purpose.

\nocite{oreily} \nocite{guide}

\bibliography{references} 
\bibliographystyle{acm}

