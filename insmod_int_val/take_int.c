#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/stat.h>

MODULE_LICENSE("GPL");

MODULE_AUTHOR("S Pawan Kumar");

MODULE_DESCRIPTION("A simple example Linux module which accepts a parameter via the insmod command.");

MODULE_VERSION("0.01");

static int val=20;

module_param(val, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(val, "An integer");

static int __init init_func(void) {
 printk(KERN_INFO "Hello, World! The value passed is %d.\n",val); 
 return 0;
}
static void __exit exit_func(void) {
 printk(KERN_INFO "Goodbye, World!\n");
}
module_init(init_func);
module_exit(exit_func);
