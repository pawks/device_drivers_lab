#!/usr/bin/env python3
from pathlib import Path
import argparse
import os
import sys
import pyperclip
from pysqlcipher3 import dbapi2 as sqlite
APP_NAME = 'pman'
DB_NAME = 'password_data'
DB_FILE_NAME = 'password_data.db'
global db
import re

def password_strength(password: str):
    password_scores = {0:'Horrible', 1:'Weak', 2:'Medium', 3:'Strong'}
    password_strength = dict.fromkeys(['has_upper', 'has_lower', 'has_num'], False)
    if re.search(r'[A-Z]', password):
        password_strength['has_upper'] = True
    if re.search(r'[a-z]', password):
        password_strength['has_lower'] = True
    if re.search(r'[0-9]', password):
        password_strength['has_num'] = True

    score = len([b for b in password_strength.values() if b])
    if len(password) < 8:
        return password_scores[0]
    else:
        return password_scores[score]

import textwrap as _textwrap
class MultilineFormatter(argparse.HelpFormatter):
    def _fill_text(self, text, width, indent):
        text = self._whitespace_matcher.sub(' ', text).strip()
        paragraphs = text.split('|n ')
        multiline_text = ''
        for paragraph in paragraphs:
            formatted_paragraph = _textwrap.fill(paragraph, width, initial_indent=indent, subsequent_indent=indent) + '\n\n'
            multiline_text = multiline_text + formatted_paragraph
        return multiline_text
def to_data_dir():
    p = data_path()
    if not p.exists():
        p.mkdir()
    else:
        os.chdir(p)

def data_path():
    return Path.home().joinpath('.' + APP_NAME)


class SqliteHandle:
    def __init__(self, dbnam: str):
        self.dbnam = dbnam
    def __enter__(self):
        try:
            conn = sqlite.connect(self.dbnam)
            c = conn.cursor()
            import getpass
            password = getpass.getpass("enter DB password:")
            c.execute(f"PRAGMA key = \'password\'")
            self.conn = conn
            self.c = c
        except sqlite.DatabaseError:
            print("Wrong Database Password!!!")
            sys.exit(1)
        return self
    def __exit__(self,exc_type, exc_value, traceback):
        self.conn.close()
    def execute(self, sql, params=None):
        try:
            if params:
                res = self.c.execute(sql, params)
            else:
                res = self.c.execute(sql)
            self.conn.commit()
            return res
        except sqlite.IntegrityError as msg:
            if 'UNIQUE constraint failed' in str(msg):
                print("Password already exists.")
                sys.exit(1)



def init():
    import getpass
    password = getpass.getpass("enter manage key_password:")
    p = data_path()
    to_data_dir()
    if p.joinpath("key.pem").exists():
        raise Exception("key exist!!! remove it if you want to abandon data")
    import subprocess
    subprocess.call(f" openssl genrsa -aes128 -passout pass:{password} -out key.pem", shell=True)
    subprocess.call(f"openssl rsa -passin pass:{password} -in key.pem -out key.pub -pubout", shell=True)
    execute_sql(f"""
    CREATE TABLE {DB_NAME}(
   id INTEGER PRIMARY KEY   AUTOINCREMENT,
   domain           VARCHAR(100)    NOT NULL,
   account          VARCHAR(100)        NOT NULL,
   password        TEXT NOT NULL ,
   remark  TEXT
    );
    """)
    execute_sql(f"""
    CREATE UNIQUE INDEX domain_account_unique
on {DB_NAME} (domain,account);
    """)

def add(domain: str, account: str,remark:str="null"):
    to_data_dir()
    res = fetchone(f"select password from {DB_NAME} where domain=? and account=?", (domain, account))
    if not res:

        import getpass
        password = getpass.getpass("enter the password:")
        print("Password strength for domain:{0} account:{1} is {2}".format(domain,account,password_strength(password)))
        import subprocess
        status, output = subprocess.getstatusoutput(
            f'bash -c \'openssl rsautl -in <(echo "{password}")   -pubin -inkey key.pub -encrypt  | base64\'')
        if status != 0:
            raise Exception("open ssl encrypt err output:" + output)
        execute_sql(f"""
        insert into {DB_NAME}(domain,account,password,remark)VALUES(?,?,?,?)
        """,
                    (domain, account, output,remark)
                    )
    else:
        print(f"Password already exists for exist domain:{domain} account:{account}")
        raise sys.exit()

def generate_data(domain: str, account: str , length: int, pass_class: str,remark:str = 'null'):
    to_data_dir()
    res = fetchone(f"select password from {DB_NAME} where domain=? and account=?", (domain, account))
    if not res:
        import string
        password_classes = {
            'alpha': string.ascii_letters,
            'num' : string.digits,
            'alphanum': string.ascii_letters+string.digits,
            'spalphanum': string.ascii_letters+string.digits+string.punctuation
        }
        import random
        password =''.join(random.choice(password_classes[pass_class]) for i in range(length))
        print("Password strength for domain:{0} account:{1} is {2}".format(domain,account,password_strength(password)))
        import subprocess
        status, output = subprocess.getstatusoutput(
            f'bash -c \'openssl rsautl -in <(echo "{password}")   -pubin -inkey key.pub -encrypt  | base64\'')
        if status != 0:
            raise Exception("open ssl encrypt err output:" + output)
        execute_sql(f"""
        insert into {DB_NAME}(domain,account,password,remark)VALUES(?,?,?,?)
        """,
                    (domain, account, output,remark)
                    )
        pyperclip.copy(password)
        print("Password copied to clipboard.")
    else:
        print(f"Password already exists for exist domain:{domain} account:{account}")

        query_password(domain, account)

def query_password(domain: str, account: str):
    to_data_dir()
    if not account:
        accounts = query_account(domain)
        if len(accounts) == 0:
            sys.exit(0)
        while not account:
            print("Please select account.")
            for i,entry in enumerate(accounts):
                print(str(i+1)+"  "+entry[0])
            try:
                account = accounts[int(input())-1][0]
            except IndexError:
                pass
    res = fetchone(f"select password from {DB_NAME} where domain=? and account=?", (domain, account))
    if not res:
        print(f"data not exist domain:{domain} account:{account}")
        sys.exit()
    password = res[0]
    import getpass
    key_password = getpass.getpass("enter key_password:")
    dec_password = decode_password(password, key_password)
    pyperclip.copy(dec_password)
    print("Password copied to clipboard.")


def delete_data(domain: str, account: str):
    if not account:
        accounts = query_account(domain)
        if len(accounts) == 0:
            sys.exit(0)
        while not account:
            print("Please select account.")
            for i,entry in enumerate(accounts):
                print(str(i+1)+"  "+entry[0])
            try:
                account = accounts[int(input())-1][0]
            except IndexError:
                pass
    rowcnt = execute_sql(f"delete from {DB_NAME} where domain=? and account=?", (domain, account)).rowcount
    print("{} row data del".format(rowcnt))


def decode_password(password, key_password):
    import subprocess
    status,output = subprocess.getstatusoutput(f'bash -c '
                                f'\'openssl rsautl '
                                f'-in <(base64 -d <(echo "{password}"))  '
                                f'-inkey key.pem '
                                f'-decrypt '
                                f'-passin '
                                f'pass:"{key_password}"\'')
    if status != 0:
        print("Wrong password.")
        sys.exit()
    else:
        return output


def query_account(domain: str):
    to_data_dir()
    rows = fetchall(f"select account from {DB_NAME} where domain=?", (domain,))
    return rows


def list_data(domain = None,account = None):
    to_data_dir()
    sql = f"select domain,account from {DB_NAME} where 1=1"
    if domain:
        sql += f" and domain='{domain}'"
    if account:
        sql += f" and account='{account}'"
    rows = fetchall(sql)

    print("domain    account")
    for row in rows:
        print(f"{row[0]}    {row[1]}")



def execute_sql(sql, params=None):
    global db
    return db.execute(sql, params)


def fetchone(sql, params=None):
    return execute_sql(sql, params).fetchone()


def fetchall(sql, params=None):
    return execute_sql(sql, params).fetchall()

def export_func(filename: str):
    import json
    with open(filename,"w") as jsonfile:
        to_data_dir()
        rows = fetchall(f"select domain,account,password,remark from {DB_NAME}")
        data = [list(row) for row in rows]
        import getpass
        key_password = getpass.getpass("enter key_password:")

        for i,entry in enumerate(data):
            data[i][2] = decode_password(entry[2], key_password)
        json.dump(data,jsonfile)
    print("Password export file: "+str(filename))

def import_func(filename: str):
    import json
    with open(filename,"r") as jsonfile:
        to_data_dir()
        data = json.load(jsonfile)
        for entry in data:
            domain = entry[0]
            account = entry[1]
            remark = entry[3]
            res = fetchone(f"select password from {DB_NAME} where domain=? and account=?", (domain, account))
            if not res:
                password = entry[2]
                print("Password strength for domain:{0} account:{1} is {2}".format(domain,account,password_strength(password)))
                import subprocess
                status, output = subprocess.getstatusoutput(
                    f'bash -c \'openssl rsautl -in <(echo "{password}")   -pubin -inkey key.pub -encrypt  | base64\'')
                if status != 0:
                    raise Exception("open ssl encrypt err output:" + output)
                execute_sql(f"""
                insert into {DB_NAME}(domain,account,password,remark)VALUES(?,?,?,?)
                """,
                            (domain, account, output,remark)
                            )
            else:
                print(f"Password already exists for exist domain:{domain} account:{account}")
                continue


def main():
    parser = argparse.ArgumentParser(formatter_class=MultilineFormatter)

    subparsers = parser.add_subparsers(dest='command')

    export_cmd = subparsers.add_parser('export',help = "Export the stored passwords from the database to a json file.")
    export_cmd.add_argument('-f','--filename',type = lambda p: str(Path(p).absolute()),default = "./passwords.json", help = "File path to export to. Default = ./passwords.json", metavar = "PATH")
    import_cmd = subparsers.add_parser('import',help = "Import passwords from a json file into the database.")
    import_cmd.add_argument('-f','--filename',required = True,type = lambda p: str(Path(p).absolute()),default = "./passwords.json",help="Path to the file to import from.", metavar = "PATH")

    add_cmd = subparsers.add_parser('add', help = "Add a password entry to the database.")
    add_cmd.add_argument('-d', '--domain', required = True, help = "Domain name.", metavar = "URL")
    add_cmd.add_argument('-a', '--account', required = True, help = "User/Account name.", metavar = "USERNAME")
    add_cmd.add_argument('-r', '--remark', help = 'Remarks to be added.',metavar = "NOTE",default = "null")

    gen_cmd = subparsers.add_parser('generate',formatter_class = argparse.RawTextHelpFormatter,help = "Generate a password and add entry to the database.")
    gen_cmd.add_argument('-d', '--domain', required = True, help = "Domain name.", metavar = "URL")
    gen_cmd.add_argument('-a', '--account', required = True, help = "User/Account name.", metavar = "USERNAME")
    gen_cmd.add_argument('-r', '--remark', help = 'Remarks to be added.',metavar = "NOTE",default = "null")
    gen_cmd.add_argument('-l', '--length', type = int, default = 12,help="Length of the password to be generated. Default = 10",metavar = "LEN")
    gen_cmd.add_argument('-c', '--pass_class', choices = ['alpha','num','alphanum','spalphanum'],default = 'spalphanum',help = """Class of the ascii characters to be used in the password.\nalpha = Lower and upper case alphabets.\nnum = Only numbers(0-9).\nalphanum = Numbers, Lower and Upper Case alphabets.\nspalphaum = Symbols, Numbers, Lower and Upper Case alphabets.""",metavar = "CLASS")

    list_cmd = subparsers.add_parser('list',help="List the entries in the database.")
    list_cmd.add_argument('-d', '--domain', help = "Domain name.", metavar = "URL")
    list_cmd.add_argument('-a', '--account', help = "User/Account name.", metavar = "USERNAME")
    list_cmd.add_argument('-r', '--remark', help = 'Remarks to be added.',metavar = "NOTE",default = "null")

    rm_cmd = subparsers.add_parser('rm',help="Remove an entry from the database.")
    rm_cmd.add_argument('-d', '--domain', required = True, help = "Domain name.", metavar = "URL")
    rm_cmd.add_argument('-a', '--account',  help = "User/Account name.", metavar = "USERNAME")

    query_cmd = subparsers.add_parser('query',help="Query the database for a particular password and copy it to clipboard.")
    query_cmd.add_argument('-d', '--domain', required = True, help = "Domain name.", metavar = "URL")
    query_cmd.add_argument('-a', '--account', help = "User/Account name.", metavar = "USERNAME")

    init_cmd = subparsers.add_parser('init',help = "Initialise the password manager.")

    args = parser.parse_args()

    to_data_dir()

    global db
    with SqliteHandle(os.path.join(data_path(),DB_FILE_NAME)) as db:


        if args.command == 'list':
            list_data(domain=args.domain,account=args.account)
        elif args.command == 'add':
            add(args.domain, args.account,args.remark)
        elif args.command == 'rm':
            delete_data(args.domain, args.account)
        elif args.command == 'generate':
            generate_data(args.domain,args.account,args.length,args.pass_class,args.remark)
        elif args.command == 'query':
            query_password(args.domain, args.account)
        elif args.command == 'init':
            init()
        elif args.command == 'export':
            export_func(args.filename)
        elif args.command == 'import':
            import_func(args.filename)
        else:
            raise Exception("something wrong happen! command:"+args.command)

if __name__=="__main__":
    main()
