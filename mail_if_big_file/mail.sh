#!/usr/bin/sh
config=./config.ini
to=te556ob980@pm.me
message=./message.txt
text=`find $PWD/ -type f -size +100M -exec ls -lh -p {} \; | cut -d' ' --fields=5,9;`
if test -n "$text"
then
    subject="File size warning on `date`.";
    echo $text>$message;
    python send_mail.py --config $config --to $to --subject "$subject" --message $message;
fi
