import smtplib
from email.mime.text import MIMEText
import configparser
import argparse
import pathlib

parser = argparse.ArgumentParser()
parser.add_argument('--cc',action='store')
parser.add_argument('--to',action='store')
parser.add_argument('--subject',action='store')
parser.add_argument('--message',action='store',type= lambda p: str(pathlib.Path(p).absolute()))
parser.add_argument('--config',action='store',type= lambda p: str(pathlib.Path(p).absolute()))
args = parser.parse_args()
config = configparser.ConfigParser()
config.read(args.config)
config = config['mail']
smtp_ssl_host = 'smtp.gmail.com'  # smtp.mail.yahoo.com
smtp_ssl_port = 465
username = config['username']
password = config['password']
sender = config['sender']

with open(args.message,"r") as file:
    filetext = file.read()
msg = MIMEText(filetext)
msg['Subject'] = args.subject
msg['From'] = sender
msg['To'] = args.to
if args.cc:
    msg['Cc'] = args.cc

server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)
server.login(username, password)
server.sendmail(sender, args.to, msg.as_string())
server.quit()

