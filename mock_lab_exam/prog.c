#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    char *args[]={"grep","d$",NULL};
    pid_t pid;
    int pipefd[2];
    int status;
    pipe(pipefd);
    pid = fork();
    if (pid == 0) {
        char *args2[] = {"ps","-eo","comm",NULL};
        dup2(pipefd[1], STDOUT_FILENO);
        close(pipefd[0]);
        close(pipefd[1]);
        if (execvp(args2[0],args2) == -1)
            perror("execl");
    } else {
        wait(&status);
        if (fork() == 0) {
            dup2(pipefd[0], STDIN_FILENO);
            close(pipefd[0]);
            close(pipefd[1]);
            if (execvp(args[0],args) == -1)
                perror("execvp grep");
        } else {
            wait(&status);
            close(pipefd[0]);
            close(pipefd[1]);
            
        }
    }
}

